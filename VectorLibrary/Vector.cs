﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorLibrary
{
    public class Vector : IComparable<Vector>
    {
        public double X { get; private set; }
        public double Y { get; private set; }
        public double Z { get; private set; }

        /// <summary>
        /// Constructor taking three parameters
        /// </summary>
        /// <param name="x">Double value on X axis</param>
        /// <param name="y">Double value on Y axis</param>
        /// <param name="z">Double value on Z axis</param>
        public Vector(double x = 0, double y = 0, double z = 0)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Calculate length of the vector
        /// </summary>
        /// <returns>Double value</returns>
        public double Length()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2));
        }

        /// <summary>
        /// Overloaded operator for addition two vectors
        /// </summary>
        /// <param name="vector1">First vector to add</param>
        /// <param name="vector2">Second vector to add</param>
        /// <returns>New vector</returns>
        public static Vector operator +(Vector vector1, Vector vector2)
        {
            if ((vector1 is null) || (vector2 is null))
                throw new ArgumentNullException("Can not add null vector");
            return new Vector(vector1.X + vector2.X, vector1.Y + vector2.Y, vector1.Z + vector2.Z);
        }

        /// <summary>
        /// Overloaded operator for subtraction two vectors
        /// </summary>
        /// <param name="vector1">First vector</param>
        /// <param name="vector2">Second vector</param>
        /// <returns>New vector</returns>
        public static Vector operator -(Vector vector1, Vector vector2)
        {
            if ((vector1 is null) || (vector2 is null))
                throw new ArgumentNullException("Can not subtract null vector");
            return new Vector(vector1.X - vector2.X, vector1.Y - vector2.Y, vector1.Z - vector2.Z);
        }

        /// <summary>
        /// Overloaded operator for multiplying scalar value and vector
        /// </summary>
        /// <param name="k">Skalar value</param>
        /// <param name="vector1">Vector</param>
        /// <returns>New vector</returns>
        public static Vector operator *(double k, Vector vector1)
        {
            if (vector1 is null)
                throw new ArgumentNullException("Can not multiply null vector");
            return new Vector(vector1.X * k, vector1.Y * k, vector1.Z * k);
        }

        /// <summary>
        /// Overloaded operator for multiplying vector and scalar value
        /// </summary>
        /// <param name="vector1">Vector</param>
        /// <param name="k">Scalar value</param>
        /// <returns>New vector</returns>
        public static Vector operator *(Vector vector1, double k)
        {
            if (vector1 is null)
                throw new ArgumentNullException("Can not multiply null vector");
            return new Vector(vector1.X * k, vector1.Y * k, vector1.Z * k);
        }

        /// <summary>
        /// Overrided function Equals
        /// </summary>
        /// <param name="obj">Incoming parameter</param>
        /// <returns>True, if objects are equal</returns>
        public override bool Equals(object obj)
        {
            Vector vector = obj as Vector;
            if (vector is null)
                return false;
            else
                return ((this.X == vector.X) && (this.Y == vector.Y) && (this.Z == vector.Z));
        }

        /// <summary>
        /// Overloaded operator for comparison two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>True, if vectors are equal</returns>
        public static bool operator ==(Vector v1, Vector v2)
        {
            if (v1 is null)
                throw new ArgumentNullException("Can not comparise null vector");
            return v1.Equals(v2);
        }

        /// <summary>
        /// Overloaded operator for comparison two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>True, if vectors are not equal</returns>
        public static bool operator !=(Vector v1, Vector v2)
        {
            if (v1 is null)
                throw new ArgumentNullException("Can not comparise null vector");
            return !v1.Equals(v2);
        }

        /// <summary>
        /// Represents vector in string format
        /// </summary>
        /// <returns>String value</returns>
        public override string ToString()
        {
            return string.Format("X:{0};\t Y:{1};\t Z:{2};", X, Y, Z);
        }

        /// <summary>
        /// Compare this vector with another
        /// </summary>
        /// <param name="v">Vector to compare</param>
        /// <returns>Int value</returns>
        public int CompareTo(Vector v)
        {
            if (v is null)
                throw new ArgumentNullException("Can not compare null vector");
            if (this.Length() > v.Length())
                return 1;
            if (this.Length() < v.Length())
                return -1;
            else
                return 0;
        }

        /// <summary>
        /// Overloaded operator for comparison two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>True, if first vector is smaller than second</returns>
        public static bool operator <(Vector v1, Vector v2)
        {
            if (v1 is null)
                throw new ArgumentNullException("First vector can not be equal to null");
            return (v1.CompareTo(v2) < 0);
        }

        /// <summary>
        /// Overloaded operator for comparing two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>True, if first vector is greater than second</returns>
        public static bool operator >(Vector v1, Vector v2)
        {
            if (v1 is null)
                throw new ArgumentNullException("First vector can not be equal to null");
            return (v1.CompareTo(v2) > 0);
        }

        /// <summary>
        /// Overloaded operator for comparing two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>True, if first vector is less than or equal to second</returns>
        public static bool operator <=(Vector v1, Vector v2)
        {
            if (v1 is null)
                throw new ArgumentNullException("First vector can not be equal to null");
            return (v1.CompareTo(v2) <= 0);
        }

        /// <summary>
        /// Overloaded operator for comparing two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>True, if first vector is bigger than or equal to second</returns>
        public static bool operator >=(Vector v1, Vector v2)
        {
            if (v1 is null)
                throw new ArgumentNullException("First vector can not be equal to null");
            return (v1.CompareTo(v2) >= 0);
        }

        /// <summary>
        /// Calculates scalar product of two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>Scalar product</returns>
        public static double ScalarProduct(Vector v1, Vector v2)
        {
            if (v1 is null || v2 is null)
                throw new ArgumentNullException("Vectors can not be equal to null");
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }

        /// <summary>
        /// Calculates vector product of two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>Vector product</returns>
        public static Vector VectorProduct(Vector v1, Vector v2)
        {
            if (v1 is null || v2 is null)
                throw new ArgumentNullException("Vectors can not be equal to null");
            return new Vector(v1.Y * v2.Z - v1.Z * v2.Y, v1.Z * v2.X - v1.X * v2.Z, v1.X * v2.Y - v1.Y * v2.X);
        }

        /// <summary>
        /// Calculates mixed product of two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>Mixed product</returns>
        public static double MixedProduct(Vector v1, Vector v2, Vector v3)
        {
            if (v1 is null || v2 is null || v3 is null)
                throw new ArgumentNullException("Vectors can not be equal to null");
            return (v1.X * v2.Y * v3.Z) + (v1.Y * v2.Z * v3.X) + (v1.Z * v2.X * v3.Y) - (v1.Z * v2.Y * v3.X) - (v1.Y * v2.X * v3.Z) - (v1.X * v2.Z * v3.Y);
        }

        /// <summary>
        /// Calculates angle between two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns>Double value</returns>
        public static double AngleOfVectors(Vector v1, Vector v2)
        {
            if (v1 is null || v2 is null)
                throw new ArgumentNullException("Vectors can not be equal to null");
            double cos = ScalarProduct(v1, v2) / (v1.Length() * v2.Length());
            return Math.Acos(cos);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VectorLibrary;

namespace Task1_Vector
{
    class Program
    {
        static void Main(string[] args)
        {
            TestVectorLibrary();
        }

        static void TestVectorLibrary()
        {
            try
            {
                Vector v1 = new Vector(3, 4, 5);
                Vector v2 = new Vector(1, 1, 1);
                Console.WriteLine("Vector1: {0}", v1);
                Console.WriteLine("Vector2: {0}", v2);
                Console.WriteLine();
                Console.WriteLine("Vector2 length: {0}", v2.Length());
                Console.WriteLine($"Vector1 + vector2: {v1+v2}");
                Console.WriteLine($"Vector1 - vector2: {v1-v2}");
                Console.WriteLine($"Vector1 * 5: {v1*5}");
                Console.WriteLine($"Is vector1 equal to vector2: {v1==v2}");
                Console.WriteLine($"Is vector2 less than vector1: {v2<v1}");
                Console.WriteLine($"Vector product of vector1 and vector2: {Vector.VectorProduct(v1,v2)}");
                Console.WriteLine($"Angle between vectors: {Vector.AngleOfVectors(v1,v2)}");
                Vector v3 = new Vector(3, 7, 6.5);
                Console.WriteLine($"Vector3: {v3}");
                Console.WriteLine($"Mixed product of vectors: {Vector.MixedProduct(v1,v2,v3)}");
                Console.WriteLine();
                Console.WriteLine("Application run successfully");
                Console.WriteLine("Press enter to exit");
            }
            catch(ArgumentNullException argEx)
            {
                Console.WriteLine("Application crashed!");
                Console.WriteLine(argEx.Message);
            }
            catch(ArgumentException argEx)
            {
                Console.WriteLine("Application crashed!");
                Console.WriteLine(argEx.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
